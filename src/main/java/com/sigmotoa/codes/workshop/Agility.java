package com.sigmotoa.codes.workshop;

import java.util.Arrays;
import java.util.Collection;

/**
 * @author sigmotoa
 *
 * This class contains some
 * popular brain games with numbers
 */
public class Agility {

    //bigger than

    //Show if the first number is bigger than the second
    public static boolean biggerThan(String numA, String numB)
    {
        boolean Resultado;
        double NumeroA,NumeroB;

        NumeroA = Double.parseDouble(numA);
        NumeroB = Double.parseDouble(numB);

        Resultado=false;

        if (NumeroA > NumeroB)
        {
            Resultado = true;
        }

        return Resultado;
        //solved code
    }

    //Sort from bigger the numbers an show in array
    public static int[] order(int numA, int numB, int numC,int numD,int numE)
    {
        int[] numero = new int[]{numA, numB, numC, numD, numE};

        Arrays.sort(numero);

        return numero;
        //solved code
    }

    //Look for the smaller number of the array
    public static double smallerThan(double array [])
    {
        double minimo;
        minimo = array[0];

        for(int x = 0; x < array.length; x++)
        {
            if(array[x] < minimo)
            {
                minimo = array[x];
            }
        }

        return minimo;
        //solved code
    }

    //Palindrome number is called in Spanish capicúa
    //The number is palindrome
    public static boolean palindromeNumber(Integer numA)
    {
        int rNumeroA;
        boolean resultado;

        resultado = false;

        rNumeroA = Integer.reverse(numA);
        if(rNumeroA == numA)
        {
            resultado = true;
        }
        return resultado;
        //unsolved code
    }

    //the word is palindrome
    public static boolean palindromeWord(String word)
    {
        //unsolved code
        return false;
    }

    //Show the factorial number for the parameter
   public static int factorial(int numA)
   {
       int factorial = 1;

       if(numA == 0)
       {
           factorial = 1;
       }
       else
       {
           for(int x = 1; x <= numA; x++)
           {
               factorial = factorial*x;
           }
       }
       return factorial;
       //solved code
   }

   //is the number odd
   public static boolean isOdd(byte numA)
   {
       boolean resultado;
       resultado = true;

       if ((numA % 2) == 0)
       {
           resultado = false;
       }

       return resultado;
       //solved code
   }

   //is the number prime
   public static boolean isPrimeNumber(int numA)
   {
       boolean resultado;
       int MOD_num;

       MOD_num = 0;
       resultado = false;

       //this code validates how many divisors does the number have
       for (int x = 1; x <= numA; x++)
       {
           if((numA % x) == 0)
           {
               MOD_num = MOD_num + 1;
           }
       }

       //if the number have 2 or less divisors means the number is prime
       if (MOD_num == 2)
       {
           resultado = true;
       }
       return resultado;
       //solved code
   }

   //is the number even
    public static boolean isEven(byte numA)
    {
        boolean resultado;

        resultado = false;

        if((numA % 2) == 0)
        {
            resultado = true;
        }

        return resultado;
        //solved code
    }

    //is the number perfect
    public static boolean isPerfectNumber(int numA)
    {
        boolean resultado;
        int sum;

        sum = 0;
        resultado = false;

        for(int x =1; x < numA; x++)
        {
            //This section defines the divisors of the number and sum them
            if((numA % x) == 0)
            {
                sum = sum + x;
            }

            if(sum == numA)
            {
                resultado = true;
            }
        }

        return resultado;
        //solved code
    }

    //Return an array with the fibonacci sequence for the requested number
    public static int [] fibonacci(int numA)
    {
        int[] secuence;

        secuence = new int [numA+1];

        secuence[0] = 0;
        secuence[1] = 1;
        if(numA >= 2)
        {
            for (int x = 2; x < secuence.length;x++)
            {
                secuence[x] = secuence[x - 1] + secuence[x - 2];
            }
        }
        return secuence;
        //solved code
    }

    //how many times the number is divided by 3
    public static int timesDividedByThree(int numA)
    {
        int times;

        times = 0;

        for(int x = 1; x <= numA; x++)
        {
            if((x % 3) == 0)
            {
                times = times +  1;
            }
        }
        return times;
    }

    //The game of fizzbuzz
    public static String fizzBuzz(int numA)
    /**
     * If number is divided by 3, show fizz
     * If number is divided by 5, show buzz
     * If number is divided by 3 and 5, show fizzbuzz
     * in other cases, show the number
     */
    {
        String word;

        word = Integer.toString(numA);

        if( ((numA % 3) == 0) && ((numA % 5) == 0) )
        {
            word = "FizzBuzz";
        }
        else if((numA % 3) == 0)
        {
            word = "Fizz";
        }
        else if((numA % 5) == 0)
        {
            word = "Buzz";
        }

        return word;
        //solved code
    }
}
