package com.sigmotoa.codes.workshop;
/**
 * @author sigmotoa
 *
 * @version 1
 *
 * Geometric Exercises
 */


import static java.lang.Math.*;

public class Geometric
{

    //Calculate the area for a square using one side
    public static int squareArea(int side)
    {
        int resultado;

        resultado = 0;

        if(side > 0)
        {
            resultado = side * side;
        }
        else if(side <= 0)
        {
            throw new IllegalArgumentException();
        }

        return resultado;
        //solved code
    }

    //Calculate the area for a square using two sides
    public static int squareArea(int sidea, int sideb)
    {
        int resultado;
        if(sidea > 0 && sideb > 0)
        {
            resultado = sidea * sideb;
        }
        else
        {
            throw new IllegalArgumentException();
        }
        return resultado;
        //solved code
    }

    //Calculate the area of circle with the radius
    public static double circleArea(double radius)
    {
        double resultado;
        if(radius >= 0 )
        {
            resultado = (radius*radius)*3.1416;
        }
        else
        {
            throw new IllegalArgumentException();
        }
        return resultado;
    }

    //Calculate the perimeter of circle with the diameter
    public static double circlePerimeter(int diameter)
    {
        double resultado;
        if(diameter >= 0 )
        {
            resultado = diameter*3.1416;
        }
        else
        {
            throw new IllegalArgumentException();
        }
        return resultado;
    }

    //Calculate the perimeter of square with a side
    public static double squarePerimeter(double side)
    {
        double resultado;
        if(side >= 0 )
        {
            resultado = 4*side;
        }
        else
        {
            throw new IllegalArgumentException();
        }
        return resultado;
    }

    //Calculate the volume of the sphere with the radius
    public static double sphereVolume(double radius)
    {
        double resultado;
        if(radius >= 0 )
        {
            double r3;
            float div;

            //define div to set a precise value of 3/4
            r3= radius*radius*radius;
            div = (float) 4/3;

            resultado = (div)*(3.1416*r3);
        }
        else
        {
            throw new IllegalArgumentException();
        }
        return resultado;
    }

    //Calculate the area of regular pentagon with one side
    public static float pentagonArea(int side)
    {
        float result,half_side,Area_10;
        double h,angle,rad_angle;

        angle= 36;
        Area_10 = 0;

        //this section finds the high of the pentagon
        rad_angle= Math.toRadians(angle);
        half_side = (float) side/2;
        h= (half_side)/Math.tan(rad_angle);

        if(side >= 0 )
        {
            result =  (float) ( ( (half_side*h)/2 )*10 );
        }
        else
        {
            throw new IllegalArgumentException();
        }
        return result;
    }

    //Calculate the Hypotenuse with two cathetus
    public static double calculateHypotenuse(double catA, double catB)
    {
        double resultado;
        double catetoA2,catetoB2;
        if(catA >= 0 && catB >= 0)
        {
            catetoA2 = catA*catA;
            catetoB2 = catB* catB;

            resultado = Math.sqrt(catetoA2+catetoB2);
        }
        else
        {
            throw new IllegalArgumentException();
        }
        return resultado;
    }

}
